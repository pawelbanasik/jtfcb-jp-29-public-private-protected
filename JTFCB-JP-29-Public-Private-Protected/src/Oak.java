
public class Oak extends Plant {

	public Oak () {
		
		
		// nie bedzie dzialac bo jest private
		// type = "tree"
		
		// dziala bo size protected a oak to subclass of plant
		// w tym samym package tez protected dziala
		this.size = "large";
		
		
		// no access specifier
		// works cause oak and plant in the same package
		this.height = 10;
	}
	
}
