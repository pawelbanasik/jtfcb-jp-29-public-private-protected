
public class App {

	
	// Dotyczy tylko instance variables to ponizej
	// private - only within same class
	// public - from anywhere
	// protected - same class, subclass and same package
	// no modifier - same package only
	
	
	
	
	public static void main(String[] args) {

		Plant plant = new Plant();
		System.out.println(plant.name);
		
		System.out.println(plant.ID);
		
		
		
		// tez tu nie dziala bo private
		// System.out.println(plant.type);
		
		
		// size is protected i gdyby app bylo w innym package co plant
		// to wtedy by nie dzialalo
		// System.out.println(plant.size);
		
		
		//gdyby w innym package to by nie dzialalo
		// to jest ten int bez okreslenia public private protected
		System.out.println(plant.height);
	}

}
