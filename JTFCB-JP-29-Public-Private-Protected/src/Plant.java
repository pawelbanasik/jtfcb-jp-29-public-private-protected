
public class Plant {

	
	// publiczna
	// zla praktyka
	// (nie robimy public, lepiej zeby dobrac sie do niej przez metody)
	// chowamy je przed swiatem
	public String name;

	
	
	
	// dopuszczalna praktyka bo final
	// jezeli juz publiczna to robimy stala/finalna
	// mozna jej uzyc w glownej apce
	public final static int ID = 8;
	
	
	
	protected String size;
	
	
	
	
	// nie nadalem public, private protected
	// czyli to jest na poziomie package
	int height;
	
	

	
	//poza klasa nie da sie do niej dobrac
	private String type;

	
	
	
	
	// jezeli chcesz acces do variables withion the class to
	// prefix this nie potrzebny (ale niektorzy daja)
	public Plant() {
		this.name = "Freddy";
		this.type = "plant";
		this.size = "medium";
		this.height = 8;
	}
}
